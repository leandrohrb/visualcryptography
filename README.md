## Visual Cryptography and Steganography for Digital Images and Audio Files

- Undergraduate research project - Institute of Computing - UNICAMP
- Scholarship granted by PIBIC/CNPq
- Developed method to apply visual cryptography and steganography to digital images and audio files.
- More info about the algorithm and the methodology in the [report](https://gitlab.com/leandrohrb/visualcryptography/blob/master/report.pdf).

### Scheme for Images (it's similar for audio)

- 3 input images: *cover1*, *cover2* and *secret*
  - *secret* is a RGB image and *cover1* and *cover2* are binary images
  - *cover1* and *cover2* can be grayscale or RGB images, they will automatically be transformed to binary
- output (*share1* and *share2*) will be generated
  - they are RGB images, though they don't look like
- *secret* information can be retrived only with both the shares

<img src="readme_images/scheme.png" width="550px">

<img src="readme_images/scheme_imgs.png" width="550px">

### Dependencies

| For Images | For Audio |
| ------ | ------ |
| Python3 | Python3 |
| Numpy | Numpy |
| Pillow | Scipy |

### How to run: for images

- At the folder `images_code`, run the following line. The three images have to have the same dimensions.
- Three images will be created at the folder `created_images`: `recovered.png`, `share1.png` and `share2.png`.

```
python3 main.py input_images/rgb_cat.png input_images/grayscale_bridge.png input_images/grayscale_clock.png
```

### How to run: for audio files

- At the folder `audio_code`, run the following line. The three audio files have to have the same dimensions (resolution, depth and time).
- Three audio files will be created at the folder `created_audio`: `recovered.wav`, `share1.wav` and `share2.wav`.

```
python3 main.py input_audio/faster_does_it.wav input_audio/as_i_figure.wav input_audio/backed_vibes_clean.wav
```
