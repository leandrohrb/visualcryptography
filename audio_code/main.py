''' Libraries '''
import numpy as np
import scipy.io.wavfile as wav
import os, time, sys
import random


def CreatesShares(secret, n_channels, n_samples):

    # Transforms secret to unsigned data
    secret = (secret + 32768).astype(np.uint16)

    # Gets only it's 3 most significant digits
    secret = np.round(secret/100).astype(np.uint16)

    # Transforms it from [0,655] to [0,999]
    secret = np.round(secret*(999/655)).astype(np.uint16)

    # Split secret into hundreds, tens and units
    hundreds = (secret // 100)
    tens     = (secret %  100) // 10
    units    = (secret %  100) %  10

    # Creates shares
    share1 = np.zeros(n_samples * n_channels).astype(np.int16)
    share2 = np.zeros(n_samples * n_channels).astype(np.int16)

    random_array = np.array([random.randrange(0, 10) for x in range(n_samples*n_channels)]).astype(np.uint16)
    share1 += 100 * random_array
    share2 += 100 * ((random_array + hundreds)%10)

    random_array = np.array([random.randrange(0, 10) for x in range(n_samples*n_channels)]).astype(np.uint16)
    share1 += 10 * random_array
    share2 += 10 * ((random_array + tens)%10)

    random_array = np.array([random.randrange(0, 10) for x in range(n_samples*n_channels)]).astype(np.uint16)
    share1 += random_array
    share2 += (random_array + units)%10

    return share1, share2


def CoversShare(share, cover, n_channels, n_samples):

    # Share receives the 2 most significant digits from cover
    # If it's 32 or -32, reduce it to 31 or -31, because 32999 does not exist, neither -32999
    cover = np.round(cover/1000).astype(np.int16)
    cover[cover >  31] =  31
    cover[cover < -31] = -31

    # Also keeps the signs
    index1 = cover <  0
    index2 = cover >= 0

    # First, the negative values
    share[index1] = -share[index1] + 1000*cover[index1]
    # Now, the positives
    share[index2] = share[index2] + 1000*cover[index2]


def RecoversSecret(share1, share2, n_channels, n_samples):

    # Recovers secret from share1 and share2
    recovered = np.zeros(share1.shape, np.uint16)

    # Discards the 2 most significant digits from share1 and share2
    share1 = np.absolute(share1) % 1000
    share2 = np.absolute(share2) % 1000

    hundreds = (share2 // 100) - (share1 // 100)
    hundreds[hundreds<0] += 10
    hundreds = hundreds.astype(np.uint16)

    tens = ((share2 // 10) % 10) - ((share1 // 10) % 10)
    tens[tens<0] += 10
    tens = tens.astype(np.uint16)

    units = ((share2 % 100) % 10) - ((share1 % 100) % 10)
    units[units<0] += 10
    units = units.astype(np.uint16)

    # Get recovered image
    recovered = (100*hundreds + 10*tens + units).astype(np.uint16)
    recovered = np.round(recovered*(655/999)).astype(np.uint16)
    recovered = 100*recovered
    recovered = (recovered - 32768).astype(np.int16)

    return recovered


def main():

    start_time = time.time()

    # Checks if the right number of paths has been passed
    if len(sys.argv) != 4:
        print("Error: wrong number of arguments. This program requires the path to the following images, in this exactly order: the secret audio, the audio that will cover share1 and the one that will be cover share2. They all must be .wav")
        exit()
    secret_path = str(sys.argv[1])
    cover1_path = str(sys.argv[2])
    cover2_path = str(sys.argv[3])

    # Checks if the paths are valid
    if (not os.path.isfile(secret_path)
        or (not os.path.isfile(cover1_path))
        or (not os.path.isfile(cover2_path))):
        print("Error: One or more paths are wrong.")
        exit()

    # Paths for the audios that will be created and saved
    recovered_path  = "created_audio/recovered.wav"
    share1_path     = "created_audio/share1.wav"
    share2_path     = "created_audio/share2.wav"

    # Opens secret audio
    secret_freq_rate, secret = wav.read(secret_path)

    # Opens cover1 audio and cover2 audio
    cover1_freq_rate, cover1 = wav.read(cover1_path)
    cover2_freq_rate, cover2 = wav.read(cover2_path)

    # Checks sizes
    if (cover1.shape != cover2.shape or cover1.shape != secret.shape):
        print("Error: audios must have the same amount of data.")
        exit()

    # Reshapes audios
    n_samples, n_channels = secret.shape[0], secret.shape[1]
    secret = secret.reshape(n_channels * n_samples)
    cover1 = cover1.reshape(n_channels * n_samples)
    cover2 = cover2.reshape(n_channels * n_samples)

    # Creates 2 shares from the secret audio: share1 and share2
    share1, share2 = CreatesShares(secret, n_channels, n_samples)

    # Covers shares
    CoversShare(share1, cover1, n_channels, n_samples)
    CoversShare(share2, cover2, n_channels, n_samples)

    # Saves share1 and share2
    wav.write(share1_path, secret_freq_rate, share1.reshape(n_samples, n_channels))
    wav.write(share2_path, secret_freq_rate, share2.reshape(n_samples, n_channels))

    # Recovers secret from share1 and share2
    recovered = RecoversSecret(share1, share2, n_channels, n_samples)

    # Saves recovered audio
    wav.write(recovered_path, secret_freq_rate, recovered.reshape(n_samples, n_channels))

    end_time = time.time()
    print("runtime: %f seconds" %(end_time - start_time))


''' Calling main function '''
main()
