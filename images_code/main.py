''' Libraries '''
from PIL import Image
import numpy as np
import os, time, sys
import random


def CreatesShares(secret, width, height, bands):

    # Both share1 and share2 have the same dimensions of the secret
    share1 = np.empty((width*height*bands), np.uint8)
    share2 = np.empty((width*height*bands), np.uint8)

    # Transform pixels from [0,255] to [0,99]
    secret = np.round((99/255)*secret).astype(np.uint8)

    # share1 and share2 receives random integers from [0,9] in its units digit
    share1 = np.array([random.randrange(0, 10) for x in range(width*height*bands)]).reshape((width*height, bands)).astype(np.uint8)
    share2 = np.array([random.randrange(0, 10) for x in range(width*height*bands)]).reshape((width*height, bands)).astype(np.uint8)

    # share1 receives (ten digit from secret + random at share2) % 10 in its tens digit
    share1 += 10 * ((secret // 10 + share2  % 10) % 10)

    # share2 receives (unit digit from secret + random at share1) % 10 in its tens digit
    share2 += 10 * ((secret % 10 + share1 % 10) % 10)

    # Rerturns the shares
    return (share1, share2)


def CoversShare(share, bin_img, height, width, bands):

    # Covers each band with the same bin image
    for i in range(0, bands):

        # Gets all index from pixels that are greater or equal to 55
        index1 = [share[:,i] >= 55]
        # Gets all index from pixels that are lesser than 55
        index2 = [share[:,i] <  55]

        # The ones that are greater or equal to 55, receives += 100 if the bin_img pixel is white
        share[:,i][index1] += 100*bin_img[index1]
        # The ones that are lesser than 55, receives += 200 if the bin_img pixel is white
        share[:,i][index2] += 200*bin_img[index2]


def RecoversSecret(share1, share2, width, height, bands):

    # Secret recovered from shares
    recovered = np.empty((width*height, bands), np.uint8)

    # "Tens" and "units" digits (they belong to [-9, 9])
    tens  = np.array(((share1 // 10) % 10) - (share2 % 10)).astype(np.int8)
    units = np.array(((share2 // 10) % 10) - (share1 % 10)).astype(np.int8)

    # Correct negative values
    tens[tens<0] += 10
    units[units<0] += 10

    # Computes pixel value and convert it from [0,99] back to [0,255]
    recovered = np.round((255/99)*(10*tens + units)).astype(np.uint8)

    # Returns recovered image
    return (recovered)


def main():

    start_time = time.time()

    # Checks if the right number of paths has been passed
    if len(sys.argv) != 4:
        print("Error: wrong number of arguments. This program requires the path to the following images, in this exactly order: the secret image, the image that will be show by share1 and the image that will be show by share2.")
        exit()
    secret_path = str(sys.argv[1])
    bin1_path = str(sys.argv[2])
    bin2_path = str(sys.argv[3])

    # Checks if the paths are valid
    if (not os.path.isfile(secret_path)
        or (not os.path.isfile(bin1_path))
        or (not os.path.isfile(bin2_path))):
        print("Error: One or more paths are wrong.")
        exit()

    # Paths for the images that will be created and saved
    recovered_path  = "created_images/recovered.png"
    diff_path       = "created_images/diff.png"
    share1_path     = "created_images/share1.png"
    share2_path     = "created_images/share2.png"

    # Opens the secret image and converts its dimensions
    secret = np.array(Image.open(secret_path), dtype=np.uint8)

    # Image dimensions
    bands = 1 if len(secret.shape)==2 else secret.shape[2]
    height, width = secret.shape[0], secret.shape[1]

    # Reshape the secret image
    secret_original_shape = secret.shape
    secret = np.reshape(secret, (width*height, bands))

    # Opens the images that will be show by shares 1 and 2 and convert them to binary images (1 band and 1 bit per pixel)
    bin1 = np.array(Image.open(bin1_path).convert("1"), np.uint8)
    bin2 = np.array(Image.open(bin2_path).convert("1"), np.uint8)

    # Checks images sizes
    if (bin1.shape != bin2.shape or bin1.shape[0]*bin1.shape[1] != width*height):
        print("Error: images must have the same width and height.")
        exit()

    # Reshape binary images
    bin1 = bin1.reshape(height*width)
    bin2 = bin2.reshape(height*width)

    # Creates 2 shares from the secret image: share1 and share2
    share1, share2 = CreatesShares(secret, width, height, bands)

    # Covers all bands of share1 with bin1 and share2 with bin2 (bin1 and bin2 are single band binary images)
    CoversShare(share1, bin1, height, width, bands)
    CoversShare(share2, bin2, height, width, bands)

    # Saves the shares
    Image.fromarray(share1.reshape(secret_original_shape)).save(share1_path)
    Image.fromarray(share2.reshape(secret_original_shape)).save(share2_path)

    end_time = time.time()
    print("runtime: %f seconds" %(end_time - start_time))
    start_time = time.time()

    # Recovers the secret from the shares
    recovered = RecoversSecret(share1, share2, width, height, bands)

    # Saves the recovered image
    Image.fromarray(recovered.reshape(secret_original_shape)).save(recovered_path)

    end_time = time.time()
    print("runtime: %f seconds" %(end_time - start_time))


''' Run main function '''
main()
